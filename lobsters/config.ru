# This file is used by Rack-based servers to start the application.
require 'zipkin-tracer'

require ::File.expand_path('../config/environment',  __FILE__)

zipkin_config = {
  service_name: 'lobsters_vga',
  service_port: 3000,
  sample_rate: 1,
  sampled_as_boolean: false,
  log_tracing: true,
  json_api_host: "http://#{Lobsters::Application.config.tracing_host}/api/v1/spans"
}

use ZipkinTracer::RackHandler, zipkin_config

run Rails.application
