class HatRequests
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  attr_accessor :requests
  def persisted?
    false
  end
  def initialize(hRequests)
    @requests = hRequests
  end
  # include Roar::JSON
  # collection :requests
end