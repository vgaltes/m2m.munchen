class HatRequestPrinter extend ActiveModel::Naming
  include ActiveModel::Conversion
  def persisted?
    false
  end
  def initialize()
    @user = "cocotero"
    @hat  = "the hat"
    @link = "the link"
    @comment = "the comment"
    @id = "123"
  end

  attr_accessor :user
  attr_accessor :hat
  attr_accessor :link
  attr_accessor :comment
  attr_accessor :id
  attr_accessor :rejection_comment

end
