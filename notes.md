docker-compose ps

docker-compose start _service_

docker-compose stop [_service_]
  stops the network or the named service, but keeps the containers

docker-compose down
  stop the network and destroy the containers

docker-compose log _service_


HATS

Routes

get "/hats" => "hats#index"
get "/hats/build_request" => "hats#build_request",
  :as => "request_hat"
post "/hats/create_request" => "hats#create_request",
  :as => "create_hat_request"
get "/hats/requests" => "hats#requests_index"
post "/hats/approve_request/:id" => "hats#approve_request",
  :as => "approve_hat_request"
post "/hats/reject_request/:id" => "hats#reject_request",
  :as => "reject_hat_request"

Controllers

hats_controller.rb
comments controller:
  - create
  - update
  - threads (?)

Models
 - hat.rb
 - comment -> use hat property (not service)
 - hat_request.rb
 - user.rb
  - grant_moderatorship_by_user!

Views (?)
 - comments/_comment
 - comments/_commentbox.html.erb
 - lobsters/app/views/hats
 - layouts/application.html.erb
 - users/show.html.erb

Schema
 - table hats
 - table hat_requests
 - table comments

// Request service

// compile
docker run -it --rm -v maven-repo:/root/.m2 -v `pwd`:/usr/src/mymaven -w /usr/src/mymaven maven:3.5.3-jdk-8-alpine mvn clean test

// build the jar
docker run -it --rm -v maven-repo:/root/.m2 -v `pwd`:/usr/src/mymaven -w /usr/src/mymaven maven:3.5.3-jdk-8-alpine mvn clean install

docker-compose up --build

<%= form_for hr, :url => approve_hat_request_url(:id => hr),
<a href="/u/<%= hr.user.username %>"><%= hr.user.username %></a>
<%= form_for hr, :url => reject_hat_request_url(:id => hr),