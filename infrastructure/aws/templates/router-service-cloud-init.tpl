#cloud-config
write_files:
  - encoding: b64
    path: /etc/telegraf/telegraf.d/telegraf.conf
    content: "${telegraf_conf_content}"
    permissions: '0644'

package_upgrade: false

packages:
  - awscli
  - nodejs

runcmd:
- wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
- sudo dpkg -i telegraf_1.6.3-1_amd64.deb
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_bucket}/${router_code}" /var/lib/router/router.tgz
- su -l -c "tar xzf /var/lib/router/router.tgz" ubuntu
- su -l -c "cd m2m.router && ZIPKIN_BASE_URL='${tracing_host}' LOBSTERS_DNS_NAME='http://${lobsters_lb_dns}' MODLOG_DNS_NAME='http://vgaltes.com' node index.js" ubuntu