InfluxDB::Rails.configure do |config|
  config.influxdb_database = "munchen"
  config.influxdb_hosts    = ["ec2-18-184-49-169.eu-central-1.compute.amazonaws.com"]
  config.influxdb_port     = 8086
end